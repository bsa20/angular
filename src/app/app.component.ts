import { Component, OnInit } from '@angular/core';
import { DateAdapter } from '@angular/material/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'angular-hw';

  constructor(private dateAdapter: DateAdapter<any>,) { }

  ngOnInit(): void {
    this.dateAdapter.setLocale('uk');
  }
}
