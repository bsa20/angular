import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { TeamDTO } from '../models/team/TeamDTO';
import { NewTeamDTO } from '../models/team/NewTeamDTO'
import { concatAll, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TeamHttpService {
  private baseUrl: string = environment.apiUrl;
  headers: HttpHeaders = null;

  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders();
    this.headers = this.headers.append('Accept', 'application/json');
  }

  public getAll(): Observable<HttpResponse<TeamDTO[]>> {
    return this.httpClient.get<TeamDTO[]>(this.baseUrl + 'team', { observe: 'response', headers: this.headers });
  }

  public delete(id: number): Observable<HttpResponse<TeamDTO>> {
    return this.httpClient.delete<TeamDTO>(this.baseUrl + `team/${id}`, { observe: 'response', headers: this.headers });
  }

  public add(project: NewTeamDTO): Observable<HttpResponse<TeamDTO>> {
    return this.httpClient.post<TeamDTO>(this.baseUrl + `team`, project, { observe: 'response', headers: this.headers });
  }

  public update(project: NewTeamDTO, id: number): Observable<HttpResponse<TeamDTO>> {
    return this.httpClient.put<TeamDTO>(this.baseUrl + `team/${id}`, project, { observe: 'response', headers: this.headers }).pipe(
      map(resp => {
        if (resp) {
          return this.getById(id);
        }
      }),
      concatAll()
    );
  }

  public getById(id: number): Observable<HttpResponse<TeamDTO>> {
    return this.httpClient.get<TeamDTO>(this.baseUrl + `team/${id}`, { observe: 'response', headers: this.headers });
  }
}
