import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { UserDTO } from '../models/user/UserDTO';
import { NewUserDTO } from '../models/user/NewUserDTO';
import { concatAll, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserHttpService {
  private baseUrl: string = environment.apiUrl;
  headers: HttpHeaders = null;

  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders();
    this.headers = this.headers.append('Accept', 'application/json');
  }

  public getAll(): Observable<HttpResponse<UserDTO[]>> {
    return this.httpClient.get<UserDTO[]>(this.baseUrl + 'user', { observe: 'response', headers: this.headers });
  }

  public delete(id: number): Observable<HttpResponse<UserDTO>> {
    return this.httpClient.delete<UserDTO>(this.baseUrl + `user/${id}`, { observe: 'response', headers: this.headers });
  }

  public add(project: NewUserDTO): Observable<HttpResponse<UserDTO>> {
    return this.httpClient.post<UserDTO>(this.baseUrl + `user`, project, { observe: 'response', headers: this.headers });
  }

  public update(project: NewUserDTO, id: number): Observable<HttpResponse<UserDTO>> {
    return this.httpClient.put<UserDTO>(this.baseUrl + `user/${id}`, project, { observe: 'response', headers: this.headers }).pipe(
      map(resp => {
        if (resp) {
          return this.getById(id);
        }
      }),
      concatAll()
    );
  }

  public getById(id: number): Observable<HttpResponse<UserDTO>> {
    return this.httpClient.get<UserDTO>(this.baseUrl + `user/${id}`, { observe: 'response', headers: this.headers });
  }
}
