import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ProjectDTO } from '../models/project/ProjectDTO';
import { NewProjectDTO } from '../models/project/NewProjectDTO';
import { map, concatAll } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProjectHttpService {
  private baseUrl: string = environment.apiUrl;
  private headers: HttpHeaders = null;

  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders();
    this.headers = this.headers.append('Accept', 'application/json');
  }

  public getAllProjects(): Observable<HttpResponse<ProjectDTO[]>> {
    return this.httpClient.get<ProjectDTO[]>(this.baseUrl + 'project', { observe: 'response', headers: this.headers });
  }

  public deleteProject(id: number): Observable<HttpResponse<ProjectDTO>> {
    return this.httpClient.delete<ProjectDTO>(this.baseUrl + `project/${id}`, { observe: 'response', headers: this.headers });
  }

  public addProject(project: NewProjectDTO): Observable<HttpResponse<ProjectDTO>> {
    return this.httpClient.post<ProjectDTO>(this.baseUrl + `project`, project, { observe: 'response', headers: this.headers });
  }

  public updateProject(project: NewProjectDTO, id: number): Observable<HttpResponse<ProjectDTO>> {
    return this.httpClient.put<ProjectDTO>(this.baseUrl + `project/${id}`, project, { observe: 'response', headers: this.headers })
      .pipe(
        map(resp => {
          if (resp) {
            return this.getById(id);
          }
        }),
        concatAll()
      );
  }

  public getById(id: number): Observable<HttpResponse<ProjectDTO>> {
    return this.httpClient.get<ProjectDTO>(this.baseUrl + `project/${id}`, { observe: 'response', headers: this.headers });
  }
}
