import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { TaskDTO } from '../models/task/TaskDTO';
import { NewTaskDTO } from '../models/task/NewTaskDTO'
import { concatAll, map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class TaskHttpService {
  private baseUrl: string = environment.apiUrl;
  headers: HttpHeaders = null;

  constructor(private httpClient: HttpClient) {
    this.headers = new HttpHeaders();
    this.headers = this.headers.append('Accept', 'application/json');
  }

  public getAll(): Observable<HttpResponse<TaskDTO[]>> {
    return this.httpClient.get<TaskDTO[]>(this.baseUrl + 'task', { observe: 'response', headers: this.headers });
  }

  public delete(id: number): Observable<HttpResponse<TaskDTO>> {
    return this.httpClient.delete<TaskDTO>(this.baseUrl + `task/${id}`, { observe: 'response', headers: this.headers });
  }

  public add(project: NewTaskDTO): Observable<HttpResponse<TaskDTO>> {
    return this.httpClient.post<TaskDTO>(this.baseUrl + `task`, project, { observe: 'response', headers: this.headers });
  }

  public update(project: NewTaskDTO, id: number): Observable<HttpResponse<TaskDTO>> {
    return this.httpClient.put<TaskDTO>(this.baseUrl + `task/${id}`, project, { observe: 'response', headers: this.headers }).pipe(
      map(resp => {
        if (resp) {
          return this.getById(id);
        }
      }),
      concatAll()
    );
  }

  public getById(id: number): Observable<HttpResponse<TaskDTO>> {
    return this.httpClient.get<TaskDTO>(this.baseUrl + `task/${id}`, { observe: 'response', headers: this.headers });
  }
}
