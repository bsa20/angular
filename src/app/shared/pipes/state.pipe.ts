import { Pipe, PipeTransform } from '@angular/core';
import { TaskState } from 'src/app/models/enums/TaskState';

@Pipe({
  name: 'state'
})
export class StatePipe implements PipeTransform {

  transform(value: number,): string {
    // switch(value){
    //   case 0: return 'Created';
    //   case 1: return 'Started';
    //   case 2: return 'Finished';
    //   case 3: return 'Canceled';
    // }

    let taskStates = Object.keys(TaskState).filter(k => isNaN(Number(k)));
    return taskStates[value];
  }

}
