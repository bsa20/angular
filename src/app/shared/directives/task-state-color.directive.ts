import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { TaskState } from 'src/app/models/enums/TaskState';

@Directive({
  selector: '[taskStateColor]'
})
export class TaskStateColorDirective implements OnInit {

  @Input() taskState: TaskState;

  constructor(private el: ElementRef) {}

  ngOnInit(): void {
    let color: string;
    switch(this.taskState){
      case 0: {
        color = "#3362d5";
        break;
      }
      case 1: {
        color = "#15c76e";
        break;
      }
      case 2: {
        color = "#46c715";
        break;
      }
      case 3: {
        color = "#c71515";
        break;
      }
    }

    this.el.nativeElement.style.color = color;
  }

}
