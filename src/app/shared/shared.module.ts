import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialComponentsModule } from '../modules/material-component/material-component.module';
import { DateUkrPipe } from './pipes/date-ukr.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { StatePipe } from './pipes/state.pipe';
import { TaskStateColorDirective } from './directives/task-state-color.directive';


@NgModule({
  declarations: [DateUkrPipe, StatePipe, TaskStateColorDirective],
  imports: [
    CommonModule,
    MaterialComponentsModule,
    ReactiveFormsModule
  ],
  exports: [CommonModule, MaterialComponentsModule, DateUkrPipe, ReactiveFormsModule, StatePipe, TaskStateColorDirective]
})
export class SharedModule { }
