export class TeamDTO{
    public id: number;
    public name: string;
    public createdAt: Date;
}