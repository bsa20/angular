import { TaskDTO } from '../task/TaskDTO';

export class ProjectDTO{
    id: number;
    name: string;
    description: string;
    createdAt: Date;
    deadline: Date;
    authorId: number;
    teamId: number;
    tasks: TaskDTO[]
}