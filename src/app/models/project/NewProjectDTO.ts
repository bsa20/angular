export class NewProjectDTO{
    public name: string;
    public description: string;
    public deadline: Date;
    public authorId: number;
    public teamId: number;
}