export class NewUserDTO{
    public firstName: string;
    public lastName: string;
    public email: string;
    public birthday: Date;
    public teamId: number;
}