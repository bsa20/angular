export class UserDTO{
    public id: number;
    public firstName: string;
    public lastName: string;
    public email: string;
    public birthday: Date;
    public registeredAt: Date;
    public teamId: number;
}