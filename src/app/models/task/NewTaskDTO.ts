import { TaskState } from '../enums/TaskState';

export class NewTaskDTO{
    public name: string;
    public description: string;
    public finishedAt: Date;
    public state: TaskState;
    public projectId: number;
    public performerId: number;
}