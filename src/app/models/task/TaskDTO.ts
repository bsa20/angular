import { TaskState } from '../enums/TaskState';

export class TaskDTO{
    public id: number;
    public name: string;
    public description: string;
    public createdAt: Date;
    public finishedAt: Date;
    public state: TaskState;
    public projectId: number;
    public performerId: number;
}