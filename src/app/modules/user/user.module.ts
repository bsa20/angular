import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { UserListComponent } from './components/user-list/user-list.component';
import { UserRoutingModule } from './user-routing.module';
import { UserItemComponent } from './components/user-item/user-item.component';



@NgModule({
  declarations: [UserListComponent, UserItemComponent],
  imports: [
    SharedModule,
    UserRoutingModule
  ]
})
export class UserModule { }
