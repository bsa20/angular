import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserListComponent } from './components/user-list/user-list.component';
import { CanCloseUnsavedGuard } from 'src/app/guards/can-close-unsaved.guard';

const routes: Routes = [
  {
    path: "users",
    component: UserListComponent,
    canDeactivate: [CanCloseUnsavedGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class UserRoutingModule { }