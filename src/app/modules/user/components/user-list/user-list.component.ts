import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { UserDTO } from 'src/app/models/user/UserDTO';
import { UserHttpService } from 'src/app/services/user-http.service';
import { environment } from 'src/environments/environment';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { ComponentCanDeactivate } from 'src/app/guards/can-close-unsaved.guard';
import { Observable } from 'rxjs';

@Component({
  selector: 'user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, ComponentCanDeactivate {
  public errorToggle: boolean = false;
  public errorMessage: string = '';
  public addToggle: boolean = false;
  public addForm: FormGroup;
  public users: UserDTO[];
  public isChildFormTouched: boolean = false;

  constructor(private userService: UserHttpService,
    private snackBarService: SnackBarService) { }

  canDeactivate(): boolean | Observable<boolean> {
    return this.addForm.untouched && !this.isChildFormTouched;
  }

  ngOnInit(): void {
    this.userService.getAll()
      .subscribe(resp => {
        if (resp) {
          this.users = resp.body;
          this.users = this.users.map(u => {
            u.registeredAt = new Date(u.registeredAt);
            u.birthday = new Date(u.birthday);
            return u;
          });
        }
      }, err => {
        this.showError(err, 'Failed to load from server');
        this.snackBarService.showErrorMessage(err.message)
      })

    this.addForm = new FormGroup({
      'firstName': new FormControl('', [
        Validators.required
      ]),
      'lastName': new FormControl('', [
        Validators.required
      ]),
      'email': new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      'birthday': new FormControl('', [
        Validators.required
      ]),
      'teamId': new FormControl(0, [
        Validators.required,
        Validators.min(1)
      ])
    })
  }

  private showError(error: any, errormsg: string) {
    this.errorToggle = true;
    this.errorMessage = errormsg;
    if (!environment.production) console.error(error);
  }

  public add() {
    if (this.addForm.valid) {
      this.userService.add(this.addForm.value)
        .subscribe(resp => {
          if (resp) {
            this.snackBarService.showErrorMessage('Added successfuly');
            this.users.unshift(resp.body as UserDTO);
            this.addForm.reset();
          }
        },
          err => {
            this.snackBarService.showErrorMessage(err.error.error);
            if (!environment.production) console.log(err);
          }
        );

      this.addToggle = false;
    }
  }

  public onChanged(changed: boolean) {
    this.isChildFormTouched = changed;
  }
}
