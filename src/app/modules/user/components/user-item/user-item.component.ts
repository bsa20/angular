import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserDTO } from 'src/app/models/user/UserDTO';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserHttpService } from 'src/app/services/user-http.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.scss']
})
export class UserItemComponent implements OnInit {
  @Input() user: UserDTO;
  @Output() onFormTouched = new EventEmitter<boolean>();

  public userExist: boolean = true;
  public showEdit: boolean = false;
  public showDescription: boolean = false;
  public updateForm: FormGroup;

  constructor(private userService: UserHttpService,
    private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.updateForm = new FormGroup({
      'firstName': new FormControl(this.user.firstName, [
        Validators.required
      ]),
      'lastName': new FormControl(this.user.lastName, [
        Validators.required
      ]),
      'email': new FormControl(this.user.email, [
        Validators.required,
        Validators.email
      ]),
      'birthday': new FormControl(this.user.birthday, [
        Validators.required
      ]),
      'teamId': new FormControl(this.user.teamId, [
        Validators.required,
        Validators.min(1)
      ])
    });

    this.updateForm.statusChanges.subscribe(x => this.formChangeState(true));
  }

  public toggleEdit(event: any = null) {
    if (event != null) event.stopPropagation();
    this.showDescription = false;
    this.showEdit = !this.showEdit;
  }

  public delete() {
    this.userService.delete(this.user.id)
      .subscribe(resp => {
        if (resp) {
          this.snackBarService.showUsualMessage('Deleted successfuly');
          this.userExist = false;
          this.formChangeState(false);
        }
      },
        err => {
          this.snackBarService.showErrorMessage('An error occurred while deleting, please try later');
          if (!environment.production) console.log(err);
        }
      );
  }

  public update() {
    if (this.updateForm.valid) {
      this.userService.update(this.updateForm.value, this.user.id)
        .subscribe(resp => {
          if (resp) {
            this.snackBarService.showUsualMessage('Updated successfuly');
            this.user = resp.body;
            this.user.registeredAt = new Date(this.user.registeredAt);
            this.user.birthday = new Date(this.user.birthday);
            this.updateForm.markAsUntouched();
            this.formChangeState(false);
          }
        },
          err => {
            this.snackBarService.showErrorMessage(err.error.error);
            if (!environment.production) console.log(err);
          });

      this.toggleEdit();
    }
  }

  public formChangeState(changed: boolean) {
    this.onFormTouched.emit(changed);
  }
}
