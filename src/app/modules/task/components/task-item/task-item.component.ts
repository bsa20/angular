import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TaskDTO } from 'src/app/models/task/TaskDTO';
import { TaskHttpService } from 'src/app/services/task-http.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { environment } from 'src/environments/environment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TaskState } from 'src/app/models/enums/TaskState';

@Component({
  selector: 'task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.scss']
})
export class TaskItemComponent implements OnInit {
  @Input() task: TaskDTO;
  @Output() onFormTouched = new EventEmitter<boolean>();

  public taskExist: boolean = true;
  public showEdit: boolean = false;
  public showDescription: boolean = false;
  public updateForm: FormGroup;
  public taskStateKeys: string[];
  public taskStates = TaskState;

  constructor(private taskService: TaskHttpService,
    private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.taskStateKeys = Object.keys(this.taskStates).filter(k => !isNaN(Number(k)));

    this.updateForm = new FormGroup({
      'name': new FormControl(this.task.name, [
        Validators.required
      ]),
      'description': new FormControl(this.task.description),
      'finishedAt': new FormControl(this.task.finishedAt, [
        Validators.required
      ]),
      'state': new FormControl(Number(this.task.state).toString(), [
        Validators.required
      ]),
      'projectId': new FormControl(this.task.projectId),
      'performerId': new FormControl(this.task.performerId, [
        Validators.required,
        Validators.min(1)
      ])
    });

    this.updateForm.statusChanges.subscribe(x => this.formChangeState(true));
  }

  toggleEdit(event: any = null) {
    if (event != null) event.stopPropagation();
    this.showDescription = false;
    this.showEdit = !this.showEdit;
  }

  public delete() {
    this.taskService.delete(this.task.id)
      .subscribe(resp => {
        if (resp) {
          this.snackBarService.showUsualMessage('Deleted successfuly');
          this.taskExist = false;
          this.formChangeState(false);
        }
      },
        err => {
          this.snackBarService.showErrorMessage('An error occurred while deleting, please try later');
          if (!environment.production) console.log(err);
        }
      );
  }

  public update() {
    if (this.updateForm.valid) {
      this.taskService.update(this.updateForm.value, this.task.id)
        .subscribe(resp => {
          if (resp) {
            this.snackBarService.showUsualMessage('Updated successfuly');
            this.task = resp.body;
            this.task.createdAt = new Date(this.task.createdAt);
            this.task.finishedAt = new Date(this.task.finishedAt);
            this.updateForm.markAsUntouched();
            this.formChangeState(false);
          }
        },
          err => {
            this.snackBarService.showErrorMessage(err.error.error);
            if (!environment.production) console.log(err);
          });

      this.toggleEdit();
    }
  }

  public formChangeState(changed: boolean) {
    this.onFormTouched.emit(changed);
  }
}
