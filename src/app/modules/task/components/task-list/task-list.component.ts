import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ProjectDTO } from 'src/app/models/project/ProjectDTO';
import { ProjectHttpService } from 'src/app/services/project-http.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { map, catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ComponentCanDeactivate } from 'src/app/guards/can-close-unsaved.guard';

@Component({
  selector: 'task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit, ComponentCanDeactivate {
  public errorToggle: boolean = false;
  public errorMessage: string = '';
  public projects: Array<ProjectDTO> = [];
  public addForm: FormGroup;
  public addToggle: boolean = false;
  public isChildFormTouched: boolean = false;

  constructor(public projectService: ProjectHttpService,
    private snackBarService: SnackBarService) { }

  canDeactivate(): boolean | Observable<boolean> {
    return !this.isChildFormTouched;
  }

  ngOnInit(): void {
    this.projectService.getAllProjects()
      .pipe(
        map(response => {
          if (response) {
            return response.body;
          }
        }),
        catchError(error => {
          return of({ isError: true, errorMessage: error.message, error: error });
        })
      )
      .subscribe(result => {
        if (result instanceof Array) {
          this.projects = result;
          this.projects = this.projects.map(p => {
            p.createdAt = new Date(p.createdAt);
            p.deadline = new Date(p.deadline);
            return p;
          });
        } else {
          this.showError((result as any).error, 'Failed to load from server');
          this.snackBarService.showErrorMessage((result as any).errorMessage);
        }
      },
        err => {
          this.showError(err, 'Failed to load from server');
          this.snackBarService.showErrorMessage(err.message)
        });
  }

  private showError(error: any, errormsg: string) {
    this.errorToggle = true;
    this.errorMessage = errormsg;
    if (!environment.production) console.error(error);
  }

  public onChanged(changed: boolean) {
    this.isChildFormTouched = changed;
  }
}
