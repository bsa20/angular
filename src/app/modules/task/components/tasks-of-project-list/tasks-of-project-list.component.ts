import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProjectDTO } from 'src/app/models/project/ProjectDTO';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TaskState } from 'src/app/models/enums/TaskState';
import { TaskHttpService } from 'src/app/services/task-http.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { TaskDTO } from 'src/app/models/task/TaskDTO';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'tasks-of-project-list',
  templateUrl: './tasks-of-project-list.component.html',
  styleUrls: ['./tasks-of-project-list.component.scss']
})
export class TasksOfProjectListComponent implements OnInit {
  @Input() project: ProjectDTO;
  @Output() onFormTouched = new EventEmitter<boolean>();

  public showDescription: boolean = false;
  public addToggle: boolean = false;
  public addForm: FormGroup;
  public taskStateKeys: string[];
  public taskStates = TaskState;

  constructor(private taskService: TaskHttpService,
    private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.taskStateKeys = Object.keys(this.taskStates).filter(k => !isNaN(Number(k)));

    this.project.tasks = this.project.tasks.map(t => {
      t.createdAt = new Date(t.createdAt);
      t.finishedAt = new Date(t.finishedAt);
      return t;
    });

    this.addForm = new FormGroup({
      'name': new FormControl('', [
        Validators.required
      ]),
      'description': new FormControl(''),
      'finishedAt': new FormControl([
        Validators.required
      ]),
      'state': new FormControl([
        Validators.required
      ]),
      'projectId': new FormControl(this.project.id),
      'performerId': new FormControl(0, [
        Validators.required,
        Validators.min(1)
      ])
    });

    this.addForm.statusChanges.subscribe(x => this.formChangeState(true));
  }

  public add() {
    if (this.addForm.valid) {
      this.taskService.add(this.addForm.value)
        .subscribe(resp => {
          if (resp) {
            this.snackBarService.showErrorMessage('Added successfuly');
            this.project.tasks.unshift(resp.body as TaskDTO);
            this.addForm.reset();
            this.formChangeState(false);
          }
        },
          err => {
            this.snackBarService.showErrorMessage(err.error.error);
            if (!environment.production) console.log(err);
          }
        );

      this.addToggle = false;
    }
  }

  public formChangeState(changed: boolean) {
    this.onFormTouched.emit(changed);
  }
}
