import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaskListComponent } from './components/task-list/task-list.component';
import { CanCloseUnsavedGuard } from 'src/app/guards/can-close-unsaved.guard';

const routes: Routes = [
  {
    path: "tasks",
    component: TaskListComponent,
    canDeactivate: [CanCloseUnsavedGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class TaskRoutingModule { }