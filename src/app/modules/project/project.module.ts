import { NgModule } from '@angular/core';
import { ProjectRoutingModule } from './project-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { ProjectListComponent } from './components/project-list/project-list.component';
import { ProjectItemComponent } from './components/project-item/project-item.component';

@NgModule({
  declarations: [ProjectListComponent, ProjectItemComponent],
  imports: [
    SharedModule,
    ProjectRoutingModule
  ],
  bootstrap: [ProjectListComponent]
})
export class ProjectModule { }
