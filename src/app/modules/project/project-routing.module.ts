import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectListComponent } from './components/project-list/project-list.component';
import { CanCloseUnsavedGuard } from 'src/app/guards/can-close-unsaved.guard';

const routes: Routes = [
  {
    path: "projects",
    component: ProjectListComponent,
    canDeactivate: [CanCloseUnsavedGuard],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class ProjectRoutingModule { }