import { Component, OnInit } from '@angular/core';
import { ProjectHttpService } from '../../../../services/project-http.service';
import { of, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ProjectDTO } from 'src/app/models/project/ProjectDTO';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { ComponentCanDeactivate } from 'src/app/guards/can-close-unsaved.guard';

@Component({
  selector: 'project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit, ComponentCanDeactivate {
  public errorToggle: boolean = false;
  public errorMessage: string = '';
  public projects: Array<ProjectDTO> = [];
  public newProjectForm: FormGroup;
  public addProjectToggle: boolean = false;
  public isChildFormTouched: boolean = false;

  constructor(public projectService: ProjectHttpService,
    private snackBarService: SnackBarService) { }

  canDeactivate(): boolean | Observable<boolean> {
    return this.newProjectForm.untouched && !this.isChildFormTouched;
  };

  ngOnInit(): void {
    this.projectService.getAllProjects()
      .pipe(
        map(response => {
          if (response) {
            return response.body;
          }
        }),
        catchError(error => {
          return of({ isError: true, errorMessage: error.message, error: error });
        })
      )
      .subscribe(result => {
        if (result instanceof Array) {
          this.projects = result;
          this.projects = this.projects.map(p => {
            p.createdAt = new Date(p.createdAt);
            p.deadline = new Date(p.deadline);
            return p;
          });
          this.sort();
        } else {
          this.showError((result as any).error, 'Failed to load from server');
          this.snackBarService.showErrorMessage((result as any).errorMessage);
        }
      },
        err => {
          this.showError(err, 'Failed to load from server');
          this.snackBarService.showErrorMessage(err.message)
        });

    this.newProjectForm = new FormGroup({
      'name': new FormControl(),
      'description': new FormControl(),
      'deadline': new FormControl([
        Validators.required
      ]),
      'authorId': new FormControl(0, [
        Validators.required,
        Validators.min(1)
      ]),
      'teamId': new FormControl(0, [
        Validators.required,
        Validators.min(1)
      ])
    });
  }

  private showError(error: any, errormsg: string) {
    this.errorToggle = true;
    this.errorMessage = errormsg;
    if (!environment.production) console.error(error);
  }

  private sort() {
    this.projects = this.projects.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
  }

  public add() {
    if (this.newProjectForm.valid) {
      this.projectService.addProject(this.newProjectForm.value)
        .subscribe(resp => {
          if (resp) {
            this.snackBarService.showErrorMessage('Added successfuly');
            this.projects.unshift(resp.body as ProjectDTO);
            this.newProjectForm.reset();
          }
        },
          err => {
            this.snackBarService.showErrorMessage(err.error.error);
            if (!environment.production) console.log(err);
          }
        );

      this.addProjectToggle = false;
    }
  }

  public onChanged(changed: boolean) {
    this.isChildFormTouched = changed;
  }
}
