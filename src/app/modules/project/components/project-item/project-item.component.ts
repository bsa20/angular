import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProjectDTO } from 'src/app/models/project/ProjectDTO';
import { ProjectHttpService } from 'src/app/services/project-http.service';
import { environment } from 'src/environments/environment';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SnackBarService } from 'src/app/services/snack-bar.service';

@Component({
  selector: 'project-item',
  templateUrl: './project-item.component.html',
  styleUrls: ['./project-item.component.scss']
})
export class ProjectItemComponent implements OnInit {
  @Input() project: ProjectDTO;
  @Output() onFormTouched = new EventEmitter<boolean>();

  public showDescription: boolean = false;
  public showEdit: boolean = false;
  public projectExist: boolean = true;
  public updateForm: FormGroup;

  constructor(public projectService: ProjectHttpService,
    private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.updateForm = new FormGroup({
      'name': new FormControl(this.project.name),
      'description': new FormControl(this.project.description),
      'deadline': new FormControl(this.project.deadline, [
        Validators.required
      ]),
      'authorId': new FormControl(this.project.authorId, [
        Validators.required,
        Validators.min(1)
      ]),
      'teamId': new FormControl(this.project.teamId, [
        Validators.required,
        Validators.min(1)
      ])
    });
    this.updateForm.statusChanges.subscribe(x => this.formChangeState(true));
  }

  public delete() {
    this.projectService.deleteProject(this.project.id)
      .subscribe(resp => {
        if (resp) {
          this.snackBarService.showUsualMessage('Deleted successfuly');
          this.projectExist = false;
          this.formChangeState(false);
        }
      },
        err => {
          this.snackBarService.showErrorMessage('An error occurred while deleting, please try later');
          if (!environment.production) console.log(err);
        }
      );
  }

  public toggleEdit(event: any = null) {
    if (event != null) event.stopPropagation();
    this.showDescription = false;
    this.showEdit = !this.showEdit;
  }

  public update() {
    if (this.updateForm.valid) {
      this.projectService.updateProject(this.updateForm.value, this.project.id)
        .subscribe(resp => {
          if (resp) {
            this.snackBarService.showUsualMessage('Updated successfuly');
            this.project = resp.body;
            this.project.createdAt = new Date(this.project.createdAt);
            this.project.deadline = new Date(this.project.deadline);
            this.updateForm.markAsUntouched;
            this.formChangeState(false);
          }
        },
          err => {
            this.snackBarService.showErrorMessage(err.error.error);
            if (!environment.production) console.log(err);
          });

      this.toggleEdit();
    }
  }

  public formChangeState(changed: boolean) {
    this.onFormTouched.emit(changed);
  }
}
