import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeamListComponent } from './components/team-list/team-list.component';
import { CanCloseUnsavedGuard } from 'src/app/guards/can-close-unsaved.guard';

const routes: Routes = [
  {
    path: "teams",
    component: TeamListComponent,
    canDeactivate: [CanCloseUnsavedGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class TeamRoutingModule { }