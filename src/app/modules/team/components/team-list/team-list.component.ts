import { Component, OnInit } from '@angular/core';
import { TeamDTO } from 'src/app/models/team/TeamDTO';
import { TeamHttpService } from 'src/app/services/team-http.service';
import { environment } from 'src/environments/environment';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ComponentCanDeactivate } from 'src/app/guards/can-close-unsaved.guard';
import { Observable } from 'rxjs';

@Component({
  selector: 'team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.scss']
})
export class TeamListComponent implements OnInit, ComponentCanDeactivate {
  public errorToggle: boolean = false;
  public errorMessage: string = '';
  public teams: Array<TeamDTO> = [];
  public addToggle: boolean = false;
  public addForm: FormGroup;
  public isChildFormTouched: boolean = false;

  constructor(private teamHttpService: TeamHttpService,
    private snackBarService: SnackBarService) { }

  canDeactivate(): boolean | Observable<boolean> {
    return this.addForm.untouched && !this.isChildFormTouched;
  }

  ngOnInit(): void {
    this.teamHttpService.getAll()
      .subscribe(resp => {
        if (resp) {
          this.teams = resp.body;
          this.teams = this.teams.map(t => {
            t.createdAt = new Date(t.createdAt);
            return t;
          });
        }
      }, err => {
        this.showError(err, 'Failed to load from server');
        this.snackBarService.showErrorMessage(err.message)
      })

    this.addForm = new FormGroup({
      'name': new FormControl('', [
        Validators.required
      ])
    });
  }

  private showError(error: any, errormsg: string) {
    this.errorToggle = true;
    this.errorMessage = errormsg;
    if (!environment.production) console.error(error);
  }

  public add() {
    if (this.addForm.valid) {
      this.teamHttpService.add(this.addForm.value)
        .subscribe(resp => {
          if (resp) {
            this.snackBarService.showErrorMessage('Added successfuly');
            this.teams.unshift(resp.body as TeamDTO);
            this.addForm.reset();
          }
        },
          err => {
            this.snackBarService.showErrorMessage(err.error.error);
            if (!environment.production) console.log(err);
          }
        );

      this.addToggle = false;
    }
  }

  public onChanged(changed: boolean) {
    this.isChildFormTouched = changed;
  }
}
