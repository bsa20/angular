import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TeamDTO } from 'src/app/models/team/TeamDTO';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TeamHttpService } from 'src/app/services/team-http.service';
import { environment } from 'src/environments/environment';
import { SnackBarService } from 'src/app/services/snack-bar.service';

@Component({
  selector: 'team-item',
  templateUrl: './team-item.component.html',
  styleUrls: ['./team-item.component.scss']
})
export class TeamItemComponent implements OnInit {
  @Input() team: TeamDTO;
  @Output() onFormTouched = new EventEmitter<boolean>();

  public teamExist: boolean = true;
  public showEdit: boolean = false;
  public showDescription: boolean = false;
  public updateForm: FormGroup;

  constructor(private teamService: TeamHttpService,
    private snackBarService: SnackBarService) { }

  ngOnInit(): void {
    this.updateForm = new FormGroup({
      'name': new FormControl(this.team.name, [
        Validators.required
      ])
    });

    this.updateForm.statusChanges.subscribe(x => this.formChangeState(true));
  }

  public toggleEdit(event: any = null) {
    if (event != null) event.stopPropagation();
    this.showDescription = false;
    this.showEdit = !this.showEdit;
  }

  public delete() {
    this.teamService.delete(this.team.id)
      .subscribe(resp => {
        if (resp) {
          this.snackBarService.showUsualMessage('Deleted successfuly');
          this.teamExist = false;
          this.formChangeState(false);
        }
      },
        err => {
          this.snackBarService.showErrorMessage('An error occurred while deleting, please try later');
          if (!environment.production) console.log(err);
        }
      );
  }

  public update() {
    if (this.updateForm.valid) {
      this.teamService.update(this.updateForm.value, this.team.id)
        .subscribe(resp => {
          if (resp) {
            this.snackBarService.showUsualMessage('Updated successfuly');
            this.team = resp.body;
            this.team.createdAt = new Date(this.team.createdAt);
            this.updateForm.markAsUntouched();
            this.formChangeState(false);
          }
        },
          err => {
            this.snackBarService.showErrorMessage(err.error.error);
            if (!environment.production) console.log(err);
          });

      this.toggleEdit();
    }
  }

  public formChangeState(changed: boolean) {
    this.onFormTouched.emit(changed);
  }
}
