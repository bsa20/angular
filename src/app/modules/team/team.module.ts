import { NgModule } from '@angular/core';
import { TeamListComponent } from './components/team-list/team-list.component';
import { TeamRoutingModule } from './team-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { TeamItemComponent } from './components/team-item/team-item.component';


@NgModule({
  declarations: [TeamListComponent, TeamItemComponent],
  imports: [
    SharedModule,
    TeamRoutingModule
  ]
})
export class TeamModule { }
